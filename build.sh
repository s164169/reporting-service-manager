#!/bin/bash
set -e
mvn clean compile assembly:single
docker build --tag reporting_manager_service .
docker image prune -f
