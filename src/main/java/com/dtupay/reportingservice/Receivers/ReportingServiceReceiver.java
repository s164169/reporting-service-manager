package com.dtupay.reportingservice.Receivers;

import com.dtupay.reportingservice.DAL.IReportingRepository;
import com.dtupay.reportingservice.DAL.Models.Report;
import com.dtupay.reportingservice.Services.abstractions.IReportingManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.TransferMoneyDTO;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.*;
//@author Mathias Boss Jørgensen
public class ReportingServiceReceiver implements EventReceiver {
    private static final String REQUEST_TOKENS_SUCCEEDED = "RequestTokensSucceeded";
    private IReportingManager reportingManager;
    private IReportingRepository reportingRepository;
    private EventSender sender;

    //New Version
    private static final String TRANSFER_MONEY_FROM_TO_SUCCEEDED = "transferMoneyFromToSucceeded";
    private static final String GET_REPORT_BY_MERCHANT_ID = "GetReportByMerchantID";
    private static final String GET_REPORT_BY_MERCHANT_ID_SUCCEEDED = "GetReportByMerchantIDSucceeded";
    private static final String GET_REPORT_BY_CUSTOMER_ID = "GetReportByCustomerID";
    private static final String GET_REPORT_BY_CUSTOMER_ID_SUCCEEDED = "GetReportByCustomerIDSucceeded";
    private static final String CREATE_MERCHANT = "CreateMerchant";
    private static final String DELETE_MERCHANT = "DeleteMerchant";
    private static final String UPDATE_MERCHANT = "UpdateMerchant";
    private static final String GET_MERCHANT_BY_ID_REPORTING = "getMerchantByIdReporting";
    private static final String GET_MERCHANT_BY_ID_SUCCESSFUL_REPORTING = "getMerchantByIdSuccessfulReporting";
    private static final String GET_TRANSACTION_REQUESTS_BY_MERCHANT_ID_REPORTING = "GetTransactionRequestsByMerchantIDReporting";
    private static final String GET_TRANSACTION_REQUESTS_BY_MERCHANT_ID_REPORTING_SUCCEEDED= "GetTransactionRequestsByMerchantIDReporting";
    private static final String REFUND_PAYMENT_SUCCEEDED = "RefundPaymentSucceeded";

    public ReportingServiceReceiver(IReportingManager reportingManager, IReportingRepository reportingRepository, EventSender sender) {

        this.reportingManager = reportingManager;
        this.reportingRepository = reportingRepository;
        this.sender = sender;
    }


    @Override
    public void receiveEvent(Event event) throws Exception {
        Gson gson = new Gson();
        Object[] args = event.getArguments();
        switch (event.getEventType()) {
            //New version
            case TRANSFER_MONEY_FROM_TO_SUCCEEDED:
            case REFUND_PAYMENT_SUCCEEDED:
                System.out.println("Handling event: " + event);
                TransferMoneyDTO transferMoneyDTO = gson.fromJson(gson.toJson(args[0]), TransferMoneyDTO.class);
                reportingManager.createReportForTransaction(transferMoneyDTO);
                break;

            case GET_REPORT_BY_MERCHANT_ID:
                System.out.println("Handling event: " + event);
                UUID merchantID =  UUID.fromString((String) args[0]);
                LocalDate startDate = gson.fromJson(gson.toJson(args[1]), LocalDate.class);
                LocalDate endDate = gson.fromJson(gson.toJson(args[2]), LocalDate.class);

                List<Report> merchantReports = reportingManager.getReportByMerchantID(merchantID, startDate, endDate);
                sendEvent(GET_REPORT_BY_MERCHANT_ID_SUCCEEDED, new Object[] {merchantReports});
                break;

            case GET_REPORT_BY_CUSTOMER_ID:
                System.out.println("Handling event: " + event);
                UUID customerID =  gson.fromJson(gson.toJson(args[0]), UUID.class);
                LocalDate startDateCustomer = gson.fromJson(gson.toJson(args[1]), LocalDate.class);
                LocalDate endDateCustomer = gson.fromJson(gson.toJson(args[2]), LocalDate.class);
                List<Report> customerReports = reportingManager.getReportByCustomerID(customerID, startDateCustomer, endDateCustomer);
                sendEvent(GET_REPORT_BY_CUSTOMER_ID_SUCCEEDED, new Object[] {customerReports});
                break;

            case REQUEST_TOKENS_SUCCEEDED:
                System.out.println("Handling event: " + event);
                Type listType = new TypeToken<ArrayList<Token>>(){}.getType();
                List<Token> yourClassList = new Gson().fromJson(gson.toJson(args[0]), listType);

                reportingRepository.addTokens(yourClassList);
                break;


            case DELETE_MERCHANT:
                System.out.println("Handling event: " + event);
                reportingManager.deleteMerchant(gson.fromJson(gson.toJson(args[0]), UUID.class));
                break;

            case CREATE_MERCHANT:
                System.out.println("Handling event: " + event);
                reportingManager.createMerchant(gson.fromJson(gson.toJson(args[0]),Merchant.class));
                break;

            case UPDATE_MERCHANT:
                System.out.println("Handling event: " + event);
                reportingManager.updateMerchant(gson.fromJson(gson.toJson(args[0]),Merchant.class));
            default:
                System.out.println("Ignoring event: "+event);
                break;

        }
    }

    private void sendEvent(String eventName, Object[] args) throws Exception {
        Event event = new Event(eventName, args);
        sender.sendEvent(event);
    }

}
