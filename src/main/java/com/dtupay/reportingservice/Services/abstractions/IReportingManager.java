package com.dtupay.reportingservice.Services.abstractions;

import com.dtupay.reportingservice.DAL.Models.Report;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.models.DTOs.TransferMoneyDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;
//@author Frederik Kirkegaard
public interface IReportingManager {
    List<Report> getReportByMerchantID(UUID merchantID, LocalDate startDate, LocalDate endDate) throws Exception;

    List<Report> getReportByCustomerID(UUID customerID, LocalDate startDateCustomer, LocalDate endDateCustomer) throws Exception;

    void createReportForTransaction(TransferMoneyDTO transferMoneyDTO);

    void deleteMerchant(UUID merchantID);

    void createMerchant(Merchant merchant);

    void updateMerchant(Merchant merchant);
}
