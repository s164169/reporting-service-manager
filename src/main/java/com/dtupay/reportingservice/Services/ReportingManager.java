package com.dtupay.reportingservice.Services;

import com.dtupay.reportingservice.DAL.IReportingRepository;
import com.dtupay.reportingservice.DAL.Models.Report;
import com.dtupay.reportingservice.Services.abstractions.IReportingManager;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.models.DTOs.TransferMoneyDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
//@author Magnus glasdam jakobsen
public class ReportingManager implements IReportingManager {

    private IReportingRepository reportingRepository;

    public ReportingManager(IReportingRepository reportingRepository) {
        this.reportingRepository = reportingRepository;
    }

    @Override
    public List<Report> getReportByMerchantID(UUID merchantID, LocalDate startDate, LocalDate endDate) throws Exception {
        return reportingRepository.getReportsByMerchantID(merchantID).stream().filter(x -> x.getDate().isAfter(startDate) && x.getDate().isBefore(endDate)).collect(Collectors.toList());
    }

    @Override
    public List<Report> getReportByCustomerID(UUID customerID, LocalDate startDate, LocalDate endDate) throws Exception {
        return reportingRepository.getReportsByCustomerID(customerID).stream().filter(x -> x.getDate().isAfter(startDate) && x.getDate().isBefore(endDate)).collect(Collectors.toList());
    }

    @Override
    public void createReportForTransaction(TransferMoneyDTO transferMoneyDTO) {
        Optional<Merchant> merchant = reportingRepository.getMerchantByBankAccount(transferMoneyDTO.fromAccount);

        if (!merchant.isPresent()) throw new NullPointerException("Merchant not found");
        Token token = reportingRepository.getToken(transferMoneyDTO.getTokenID());
        Report report = new Report(token, merchant.get().getUserID(), transferMoneyDTO.amount);
        reportingRepository.addReport(report);
    }

    @Override
    public void deleteMerchant(UUID merchantID) {
        reportingRepository.deleteMerchant(merchantID);
    }

    @Override
    public void createMerchant(Merchant merchant) {
        reportingRepository.addMerchant(merchant);
    }

    @Override
    public void updateMerchant(Merchant merchant) {
        reportingRepository.updateMerchant(merchant);
    }
}
