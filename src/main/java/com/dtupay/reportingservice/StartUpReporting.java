package com.dtupay.reportingservice;

import com.dtupay.reportingservice.DAL.IReportingRepository;
import com.dtupay.reportingservice.DAL.InMemoryRepository;
import com.dtupay.reportingservice.DAL.Models.Report;
import com.dtupay.reportingservice.Receivers.ReportingServiceReceiver;
import com.dtupay.reportingservice.Services.ReportingManager;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;
//@author Mathias Bo Jensen
public class StartUpReporting {

    public static void main(String[] args) throws Exception {
        new StartUpReporting().startUp();
    }

    private void startUp() throws Exception{
        UUID merchantID = UUID.fromString("d59d1744-5083-449d-bc7b-1384d9bf62fd");
        UUID customerID = UUID.fromString("d59d1744-5083-449d-bc7b-1384d9bf62fd");
        Merchant merchant = new Merchant("12345678", "firstname", "lastname", merchantID, "d59d1744-5083-449d-bc7b-1384d9bf62fd");
        Customer customer = new Customer("12345678", "firstname", "lastname", customerID, customerID.toString());
        Report report = new Report(new Token("testToken", customerID), merchantID, new BigDecimal(100));
        Report report2 = new Report(new Token("testToken", customerID), merchantID, new BigDecimal(100));
        report2.setDate(LocalDate.now().minusDays(5));
        IReportingRepository reportingRepository = new InMemoryRepository();
        reportingRepository.addMerchant(merchant);
        reportingRepository.addReport(report);
        reportingRepository.addReport(report2);

        EventSender eventSender = new RabbitMqSender();
        ReportingManager reportingManager = new ReportingManager(reportingRepository);
        ReportingServiceReceiver reportingServiceReceiver = new ReportingServiceReceiver(reportingManager, reportingRepository, eventSender);
        new RabbitMqListener(reportingServiceReceiver).listen();
    }
}
