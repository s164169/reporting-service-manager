package com.dtupay.reportingservice.DAL.Models;

import dtupay.core.DAL.models.Token;

import java.math.BigDecimal;
import java.rmi.server.UID;
import java.time.LocalDate;
import java.util.UUID;
//@author Mathias Bo Jensen
public class Report {
    private UUID reportID;
    private Token customerToken;
    private UUID merchantID;
    private BigDecimal amount;
    private LocalDate date;

    public Report(Token customerToken, UUID merchantID, BigDecimal amount) {
        this.reportID = UUID.randomUUID();
        this.customerToken = customerToken;
        this.merchantID = merchantID;
        this.amount = amount;
        this.date = LocalDate.now();
    }

    public Token getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(Token customerToken) {
        this.customerToken = customerToken;
    }

    public UUID getMerchant() {
        return merchantID;
    }

    public void setMerchant(UUID merchantID) {
        this.merchantID = merchantID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public UUID getReportID() {
        return reportID;
    }

    public void setReportID(UUID reportID) {
        this.reportID = reportID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
