package com.dtupay.reportingservice.DAL;

import com.dtupay.reportingservice.DAL.Models.Report;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;

import java.util.*;
import java.util.stream.Collectors;
//@author Sebastian Fisher
public class InMemoryRepository implements IReportingRepository {
    private Map<UUID, Merchant> merchants = new HashMap<>();
    private Map<UUID, Report> reports = new HashMap<>();
    private Map<String, Token> tokens = new HashMap<>();

    @Override
    public Optional<Merchant> getMerchantByBankAccount(String fromAccount) {
         return merchants.values().stream().filter(x -> !x.getAccountID().equals(fromAccount)).distinct().findFirst();
    }

    @Override
    public void addReport(Report report) {
        reports.put(report.getReportID(), report);
    }

    @Override
    public void addMerchant(Merchant merchant) {
        merchants.put(merchant.getUserID(), merchant);
    }

    @Override
    public List<Report> getReportsByMerchantID(UUID merchantID) {

        return reports.values()
                .stream()
                .filter(x ->  merchantID.equals(x.getMerchant()))
                .map(x -> new Report(new Token(x.getCustomerToken().getTokenID(), null),x.getMerchant(), x.getAmount()))
                .collect(Collectors.toList());

    }

    @Override
    public List<Report> getReportsByCustomerID(UUID customerID) {
        return reports.values().stream().filter(x ->  customerID.equals(x.getCustomerToken().getCustomerID())).collect(Collectors.toList());
    }

    @Override
    public Token getToken(String tokenID) {
        return tokens.get(tokenID);
    }

    @Override
    public void deleteMerchant(UUID merchantID) {
        merchants.remove(merchantID);
    }

    @Override
    public void addTokens(List<Token> listOfTokens) {
        for (Token token : listOfTokens) {
            tokens.put(token.getTokenID(), token);
        }
    }

    @Override
    public void updateMerchant(Merchant merchant) {
        merchants.replace(merchant.getUserID(), merchant);
    }
}
