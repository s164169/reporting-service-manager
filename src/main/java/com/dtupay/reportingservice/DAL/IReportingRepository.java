package com.dtupay.reportingservice.DAL;

import com.dtupay.reportingservice.DAL.Models.Report;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
//@author William ben embarek
public interface IReportingRepository {

    Optional<Merchant> getMerchantByBankAccount(String fromAccount);

    void addReport(Report report);

    void addMerchant(Merchant merchant);

    void deleteMerchant(UUID merchantID);

    void updateMerchant(Merchant merchant);

    List<Report> getReportsByMerchantID(UUID merchantID);

    List<Report> getReportsByCustomerID(UUID customerID);

    Token getToken(String tokenID);

    void addTokens(List<Token> listOfTokens);
}
