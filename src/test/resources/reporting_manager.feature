# new feature
# Tags: optional

Feature: Reporting Feature

  Scenario: Successful received merchant report
    Given a merchant with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    When the service receives a "GetReportByMerchantID" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then it initiates the reporting manager to create a report for the merchant
    Then the "GetReportByMerchantIDSucceeded" event is broadcast

  Scenario: Successful received customer report
    Given a customer with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    When the service receives a "GetReportByCustomerID" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then it initiates the reporting manager to create a report for the customer
    Then the "GetReportByCustomerIDSucceeded" event is broadcast

  Scenario: Delete merchant
    When the service receives a "DeleteMerchant" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then the merchant is deleted

  Scenario: Create merchant
    Given a merchant
    When the service receives a "CreateMerchant" merchant event
    Then the merchant is created

  Scenario: Update merchant
    Given a merchant
    When the service receives a "UpdateMerchant" merchant event
    Then the merchant is updated

  Scenario: Request Tokens success
    Given a token list
    When the service receives a "RequestTokensSucceeded" tokens event
    Then the tokens are added

  Scenario: Payment Succeeded
    Given a transferMoneyDTO
    When the service receives a "transferMoneyFromToSucceeded" payment event
    Then the report is created

  Scenario: Refund Succeeded
    Given a transferMoneyDTO
    When the service receives a "RefundPaymentSucceeded" payment event
    Then the report is created
