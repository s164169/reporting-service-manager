package behaviourtests;

import com.dtupay.reportingservice.DAL.IReportingRepository;
import com.dtupay.reportingservice.DAL.Models.Report;
import com.dtupay.reportingservice.Receivers.ReportingServiceReceiver;
import com.dtupay.reportingservice.Services.abstractions.IReportingManager;
import com.google.gson.Gson;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.TransferMoneyDTO;
import messaging.Event;
import messaging.EventSender;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;
//@author William ben embarek
public class ReportingManagerSteps {

    IReportingManager reportingManager = mock(IReportingManager.class);
    IReportingRepository reportingRepository = mock(IReportingRepository.class);

    EventSender eventSender = mock(EventSender.class);

    private ReportingServiceReceiver reportingServiceReceiver = new ReportingServiceReceiver(reportingManager,reportingRepository, eventSender);
    private UUID id;
    private List<Report> reportToReturn;
    private TransferMoneyDTO transaction;
    private Merchant merchant;
    private List<TransactionRequest> transactionList;
    private Gson gson = new Gson();
    private List<Token> tokenList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Given("^a merchant with ID \"([^\"]*)\"$")
    public void aMerchantWithID(String ID) throws Exception {
        id = UUID.fromString(ID);
        reportToReturn = new ArrayList<Report>();
        when(reportingManager.getReportByMerchantID(any(UUID.class), any(LocalDate.class), any(LocalDate.class))).thenReturn(reportToReturn);
    }

    @When("^the service receives a \"([^\"]*)\" event with ID \"([^\"]*)\"$")
    public void theServiceReceivesAEventWithArgs(String arg0, String arg1) throws Throwable {
        Object[] args =  {arg1, LocalDate.now(), LocalDate.now()} ;
        Event event = new Event(arg0, args);
        reportingServiceReceiver.receiveEvent(event);
    }


    @Then("^it initiates the reporting manager to create a report for the merchant$")
    public void itInitiatesTheReportingManagerToCreateAReportForTheMerchant() throws Exception {
        verify(reportingManager).getReportByMerchantID(refEq(id), any(LocalDate.class), any(LocalDate.class));
    }

    @Then("^the \"([^\"]*)\" event is broadcast$")
    public void theEventIsBroadcast(String eventType) throws Throwable {
        Object[] args = {reportingManager.getReportByMerchantID(id, LocalDate.now(), LocalDate.now())};
        Event event = new Event(eventType, args);
        verify(eventSender).sendEvent(refEq(event));
    }

    @Then("^it initiates the reporting manager to create a report for the customer$")
    public void itInitiatesTheReportingManagerToCreateAReportForTheCustomer() throws Exception {
        verify(reportingManager).getReportByCustomerID(eq(id), any(LocalDate.class), any(LocalDate.class));
    }


    @Given("^a customer with ID \"([^\"]*)\"$")
    public void aCustomerWithID(String ID) throws Throwable {
        id = UUID.fromString(ID);
        reportToReturn = new ArrayList<Report>();
        doReturn(reportToReturn).when(reportingManager).getReportByCustomerID(any(UUID.class), any(LocalDate.class), any(LocalDate.class));
    }

    @When("^the service receives a \"([^\"]*)\" merchant event$")
    public void theServiceReceivesAMerchantEvent(String arg0) throws Throwable {
        Object[] args = {merchant} ;
        Event event = new Event(arg0, args);
        reportingServiceReceiver.receiveEvent(event);
    }

    @Then("^the merchant is deleted$")
    public void theMerchantIsDeleted() {
        verify(reportingManager).deleteMerchant(any(UUID.class));
    }

    @Then("^the merchant is created$")
    public void theMerchantIsCreated() {
        verify(reportingManager).createMerchant(any(Merchant.class));

    }

    @Then("^the merchant is updated$")
    public void theMerchantIsUpdated() {
        verify(reportingManager).updateMerchant(any(Merchant.class));
    }

    @Given("^a merchant$")
    public void aMerchant() {
        merchant = new Merchant("testy","test","tesss",UUID.randomUUID(),"ssads");
        doNothing().when(reportingManager).createMerchant(any(Merchant.class));
        doNothing().when(reportingManager).updateMerchant(any(Merchant.class));
    }

    @Given("^a token list$")
    public void aTokenList() throws Exception {
        tokenList = new ArrayList<Token>();
        doNothing().when(reportingRepository).addTokens(anyListOf(Token.class));
    }

    @When("^the service receives a \"([^\"]*)\" tokens event$")
    public void theServiceReceivesATokensEvent(String arg0) throws Throwable {
        Object[] args = {tokenList} ;
        Event event = new Event(arg0, args);
        reportingServiceReceiver.receiveEvent(event);
    }

    @Then("^the tokens are added$")
    public void theTokensAreAdded() {
        verify(reportingRepository).addTokens(anyListOf(Token.class));
    }

    @Given("^a transferMoneyDTO$")
    public void aTransferMoneyDTO() throws Exception {
        transaction = new TransferMoneyDTO();
        reportToReturn = new ArrayList<Report>();
        doNothing().when(reportingManager).createReportForTransaction(any(TransferMoneyDTO.class));
    }

    @When("^the service receives a \"([^\"]*)\" payment event$")
    public void theServiceReceivesAPaymentEvent(String arg0) throws Throwable {
        Object[] args = {transaction} ;
        Event event = new Event(arg0, args);
        reportingServiceReceiver.receiveEvent(event);
    }

    @Then("^the report is created$")
    public void theReportIsCreated() {
        verify(reportingManager).createReportForTransaction(any(TransferMoneyDTO.class));
    }
}
