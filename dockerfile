FROM adoptopenjdk:8-jre-hotspot
COPY target/reporting_manager_service-jar-with-dependencies.jar /usr/src/
WORKDIR /usr/src/
CMD java -Djava.net.preferIPv4Stack=true\
         -Djava.net.preferIPv4Addresses=true\
        -jar reporting_manager_service-jar-with-dependencies.jar
